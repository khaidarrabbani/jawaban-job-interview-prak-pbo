# Nomor 1
pendekatan matematika dalam program ini terdapat pada method pembayaran. ketika memasukkan jumlah uang yang digunakan untuk pembayaran, total kembalia akan langsung ditampilkan, dengan menghitung JumlahUang - HargaTotal = Kembalian.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/1.PBO-Pendekatan_MTK.gif)
# Nomor 2
contoh pada method pesan makanan. pertama ketika user memilih pesan makanan, user akan di minta untuk memilih nomor menu, setelah user mengisi nomor menu user akan diminta untuk mengisi jumlah mekanan yang akan dipesan setelah semua diisi maka akan tampil alert "pesanan berhasil ditambahkan". jika nomor menu yang diinputkan user tidak terdapat dalam daftar menu, maka akan tampil alert "pilihan tidak tersedia"

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/2.PBO-Algoritma_solusi.gif)
# Nomor 3
Oop adalah paradigma pemrograman yang berfokus pada penggunaan objek dan kelas untuk merancang dan membangun aplikasi. konsep OOP sendiri meliputi Encapsulation, Inheritance, Polymorphism, dan Abstraction.
-Encapsulation merupakan praktik menggabungkan data dan metode yang beroperasi pada data tersebut dalam sebuah unit tunggal yang disebut objek. 
- Inheritance atau pewarisan memungkinkan objek untuk mewarisi properti dan perilaku dari objek lainnya.
- Polymorphism merujuk pada kemampuan objek dari kelas-kelas yang berbeda untuk merespons pesan atau panggilan metode yang sama dengan cara yang berbeda.
- Abstraksi melibatkan penyederhanaan sistem kompleks dengan memecahkannya menjadi komponen yang lebih mudah dikelola dan dipahami.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/2.PBO-Algoritma_solusi.gif)
# Nomor 4
Konsep enkapsulasi dapat dilihat pada penggunaan access modifiers seperti private dan public dalam deklarasi variabel dan metode.
Pada program saya Variabel daftarMenu, daftarPesanan, dan metode yang terdapat dalam kelas Restaurant di-deklarasikan sebagai private, sehingga hanya dapat diakses melalui metode publik yang tersedia.
Ini membantu untuk membatasi akses langsung ke data dan mendorong penggunaan metode untuk berinteraksi dengan objek.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/4.enkapsulation.gif)
# Nomor 5
Yang menjadi class abstrak pada program ini adalah Class Item. Kelas Item merupakan sebuah kelas abstrak yang tidak dapat diinstansiasi secara langsung.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/5._abstraction.gif)

# Nomor 6
Dalam kode di atas, terdapat penurunan kelas (inheritance) dimana Menu merupakan turunan dari kelas Item. Hal ini menunjukkan hubungan "is-a" antara Menu dan Item, yang berarti Menu adalah jenis khusus dari Item. Dengan menggunakan konsep inheritance, Menu dapat memanfaatkan atribut dan metode yang sudah ada dalam Item tanpa perlu mendefinisikannya kembali.
Dalam kode di atas pula, terdapat contoh penggunaan konsep polymorphism melalui pewarisan dan overriding.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/6._inheritance___polymorphisme.gif)
# Nomor 7
- Use Case "Menampilkan Daftar Menu":
Aktor: Pelanggan
Deskripsi: Pelanggan dapat melihat daftar menu yang tersedia di restoran.
Langkah-langkah:
a. Pelanggan memilih untuk menampilkan daftar menu.
b. Sistem menampilkan daftar menu yang tersedia.
- Use Case "Memesan Makanan":
Aktor: Pelanggan
Deskripsi: Pelanggan dapat memesan makanan dari daftar menu.
Langkah-langkah:
a. Pelanggan melihat daftar menu.
b. Pelanggan memilih menu yang ingin dipesan dan menentukan jumlahnya.
c. Pelanggan mengirim pesanan.
d. Sistem mencatat pesanan dan menambahkannya ke daftar pesanan.
- Use Case "Menampilkan Pesanan":
Aktor: Pelanggan
Deskripsi: Pelanggan dapat melihat pesanan yang telah dipesan.
Langkah-langkah:
a. Pelanggan memilih untuk menampilkan pesanan.
b. Sistem menampilkan daftar pesanan yang telah dipesan.
Use Case "Menghitung Total Harga Pesanan":
- Aktor: Sistem
Deskripsi: Sistem menghitung total harga dari pesanan yang telah dipesan.
Langkah-langkah:
a. Sistem mengumpulkan informasi tentang pesanan yang telah dipesan.
b. Sistem menghitung total harga pesanan berdasarkan harga menu dan jumlah pesanan.
Use Case "Membayar Pesanan":
- Aktor: Pelanggan
Deskripsi: Pelanggan melakukan pembayaran untuk pesanan yang telah dipesan.
Langkah-langkah:
a. Pelanggan memilih untuk membayar pesanan.
b. Pelanggan memasukkan jumlah uang yang akan dibayarkan.
c. Sistem menghitung kembalian berdasarkan total harga pesanan dan uang yang dibayarkan.
d. Sistem menampilkan kembalian kepada pelanggan.
# Nomor 8
Untuk Use Case untuk sementara masih seperti tabel di bawah. untuk rencana kedepannya fitur fitur akan diupdate lagi agar lebih lengakap dan kompleks.

![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/Usecase_tabel.png)
# Nomor 9
Berikut link video youtube demonstrasi gambaran umum aplikasi saya https://youtu.be/mE91YlqmRZs

# Nomor 10
sampai saat ini untuk ui/ux nya saya masih menggunakan cmd untuk menjalankan program. renacananya akan dikembangkan lagi agar tampilannya lebih menarik.
![](https://gitlab.com/khaidarrabbani/jawaban-job-interview-prak-pbo/-/raw/main/Jawaban%20UTS/10._Jawaban_no_10.gif)
